import "./PlayGround.scss";
import { useEffect, useState } from "react";
import SelectPlayer, { IPlayer } from "./components/selectPlayer/selectPlayer";
import { IGame, Theme } from "./Interfaces/IBingo";
import { createGame, groupBy } from "./calculations";
import Congrates from "./components/congrats/congrats";
import ThumbsUp from "./components/thumbsUp/thumbsUp";
import Sound from "react-sound";
const matrix: number = 5;
const newGame = createGame(5);

let activeGame: IGame[] = [];

const PlayGround = () => {
  const [player1Vote, setplayer1Vote] = useState<number>(0);
  const [player2Vote, setplayer2Vote] = useState<number>(0);
  const [activePlayer, setactivePlayer] = useState<string>("");
  const [isGameOver, setisGameOver] = useState(false);
  const [game, setGame] = useState<Theme[][]>(newGame);
  const [showThumbs, setshowThumbs] = useState(false)

  const [player, setPlayer] = useState<IPlayer>({
    player1: "",
    player2: ""
  });


  const setActive = (_item: Theme, row: number, col: number) => {

    if (_item.isActive) { return };

    const copy = [...newGame];
    copy.forEach((theme: Theme[]) => {
      return theme.forEach((item: Theme) => {
        if (item.id === _item.id) {
          item.isActive = true;
        }
      });
    })
    activeGame.push({
      user: activePlayer,
      row,
      col
    })
    setGame(copy);
    findBingo(activePlayer);
    setactivePlayer(player.player1 === activePlayer ? player.player2 : player.player1);
  };


  const onSetPlayers = (players: IPlayer) => {
    setPlayer(players);
    setactivePlayer(players.player1);
  };

  const setBackground = (isActive: boolean, row: number, col: number) => {
    var index = activeGame.findIndex((x: IGame) => `${row}-${col}` === `${x.row}-${x.col}`);
    return isActive ? activeGame[index].user === player.player1 ? "red" : "green" : ""
  };

  const _isGameOver = () => {
    var itemsSelected: number = document.getElementsByClassName("bingo-card__item active").length;
    return itemsSelected == 24;
  }

  useEffect(() => {
    if (player1Vote > 0) {
      setshowThumbs(true);
      setTimeout(() => {
        setshowThumbs(false);
      }, 3000);
    }
  }, [player1Vote])

  useEffect(() => {
    if (player2Vote > 0) {
      setshowThumbs(true);
      setTimeout(() => {
        setshowThumbs(false);
      }, 3000);
    }
  }, [player2Vote])

  const findBingo = (activePlayer: string) => {

    let p1_col_count = 0;
    let p2_col_count = 0;

    let p1_row_count = 0;
    let p2_row_count = 0;

    let p1_dia_count = 0;
    let p2_dia_count = 0;

    if (activePlayer === player.player1) {
      // vertical checking
      const player1Data = activeGame.filter((x: IGame) => x.user === player.player1)
      const col_grouped = groupBy(player1Data, "col");
      p1_col_count = Object.keys(col_grouped).filter((x: any) => col_grouped[x].length === 5).length;

      //row checking
      const row_grouped = groupBy(player1Data, "row");
      p1_row_count = Object.keys(row_grouped).filter((x: any) => row_grouped[x].length === 5).length;

      //diagonal calculation
      const xMatrixP1: number = player1Data.filter((x: IGame) => x.col === x.row).length;
      p1_dia_count = (xMatrixP1 === matrix) ? p1_dia_count + 1 : 0;

      const yMatrixP1: number = player1Data.filter((x: IGame) => (x.col + x.row) === 4).length;
      p1_dia_count = (yMatrixP1 === matrix) ? p1_dia_count + 1 : p1_dia_count;

      setplayer1Vote(p1_col_count + p1_row_count + p1_dia_count);

    }


    if (activePlayer === player.player2) {
      // vertical checking
      const player2Data = activeGame.filter((x: IGame) => x.user === player.player2)
      const col_grouped = groupBy(player2Data, "col");
      p2_col_count = Object.keys(col_grouped).filter((x: any) => col_grouped[x].length === 5).length;

      //row checking
      const row_grouped = groupBy(player2Data, "row");
      p2_row_count = Object.keys(row_grouped).filter((x: any) => row_grouped[x].length === 5).length;
      // setplayer2Vote(p2_col_count + p2_row_count);

      //diagonal calculation
      const xMatrixP2 = player2Data.filter((x: IGame) => x.col === x.row).length;
      p2_dia_count = (xMatrixP2 === matrix) ? p2_dia_count + 1 : 0;

      const yMatrixP2 = player2Data.filter((x: IGame) => (x.col + x.row) === 4).length;
      p2_dia_count = (yMatrixP2 === matrix) ? p2_dia_count + 1 : p2_dia_count;

      console.log(player2Data)
      setplayer2Vote(p2_col_count + p2_row_count + p2_dia_count);
    }
    if (_isGameOver()) {
      setisGameOver(_isGameOver());
    }


  }
  const getResult = () => {
    return player1Vote === player2Vote ? `Draw` :
      `Winner : ${player1Vote > player2Vote ? player.player1 : player.player2}`
  };

  const renderGame = (): JSX.Element => {
    return player.player1 && player.player2 ? <>
      <div className="outer-wrapper">
        <h1 className="big-text" style={{ display: "list-item" }}>
          <span className={player.player1 === activePlayer ? "animateActivep1" : ""}>Player1 : {player.player1}</span> ({player1Vote})
      <br></br>
          <br></br>
          <span className={player.player2 === activePlayer ? "animateActivep2" : ""}>Player2 : {player.player2}</span> ({player2Vote})
    </h1>
      </div>
      <div className="main-content">
        <div className="bingo-card">
          {game.map((themes: Theme[], row: number) => {
            return themes.map((item, col) => {
              return (
                <div className={`bingo-card__item ${item.isActive ? "active" : ""}`}
                  style={{ backgroundColor: setBackground(item.isActive, row, col) }}
                  onClick={() => { setActive(item, row, col) }}>
                  {row / 2 === 1 && col / 2 === 1 ? "0" :
                    <h3>{item.text}</h3>
                  }
                  <span className="bingo-card__checkbox" />
                </div>
              );
            });
          })}
        </div>
      </div> </> : <SelectPlayer onSetPlayers={onSetPlayers} />
  }

  return (
    <div className="App">
      <span style={{ display: showThumbs ? "initial" : "none" }}> <ThumbsUp /></span>

      {
        !isGameOver ? renderGame() : <Congrates title={getResult()} />
      }

      <Sound
        url={isGameOver ? "https://www.pacdv.com/sounds/applause-sounds/app-9.mp3" : "https://www.pacdv.com/sounds/applause-sounds/app-29.wav"}
        playStatus={(isGameOver || showThumbs) ? "PLAYING" : "STOPPED"}
        playFromPosition={300 /* in milliseconds */}
        onLoading={() => { }}
        onPlaying={() => { }}
        onFinishedPlaying={() => { }}
      />
    </div>

  );
}

export default PlayGround;
