import { useState } from "react";
import styles from "./selectPlayer.module.css";
export interface IPlayer {
    player1: string;
    player2: string;
}
interface Iprops {
    onSetPlayers: (players: IPlayer) => void;
}
const SelectPlayer = (props: Iprops) => {
    const [players, setPlayers] = useState<IPlayer>({ player1: "", player2: "" });
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setPlayers(prevState => ({
            ...prevState,
            [name]: value
        }));
    };
    const setGame = () => {
        props.onSetPlayers(players);
    }
    return (
        <div className={styles.container}>
            <div className={styles.box}>
                <h1>Select Player</h1>
                <div className={styles.row}>
                    <input value={players.player1}
                        type="text"
                        onChange={handleChange}
                        name="player1" placeholder="player 1" />
                </div>
                <div className={styles.row}>
                    <input value={players.player2}
                        type="text"
                        onChange={handleChange}
                        name="player2" placeholder="player 2" />
                </div>
                <div className={styles.row}>
                    <a style={{ cursor: "pointer" }} onClick={setGame}>Start Game</a>
                </div>

            </div>
        </div>
    )
};

export default SelectPlayer;