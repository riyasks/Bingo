import styles from "./thumbsUp.module.scss";
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faThumbsUp } from '@fortawesome/free-solid-svg-icons'
const ThumbsUp = () => {

    return (
        <div className={`${styles.center}`}>
            <a href="https://dribbble.com/shots/2527200-Like-Animation" target="_blank"
                className={`${styles.thumb} ${"fa fa-thumbs-up"}`} />
            {/* <FontAwesomeIcon icon={faThumbsUp} /> */}
            <div className={`${styles.circleWrap}`}>
                <div className={`${styles.circleLg}`} />
            </div>
            <div className={styles.dotsWrap}>
                <div className={`${styles.dot} ${styles.dott}`} />
                <div className={`${styles.dot} ${styles.dottr}`} />
                <div className={`${styles.dot} ${styles.dotbr}`} />
                <div className={`${styles.dot} ${styles.dotb}`} />
                <div className={`${styles.dot} ${styles.dotbl}`} />
                <div className={`${styles.dot} ${styles.dottl}`} />
            </div>
        </div>
    )
}

export default ThumbsUp;

// {`${styles.thumb} ${styles.fa} ${styles.thumb}`}