import styles from "./congrats.module.scss";
interface Iprops {
    title: string;
}
const n = 13;
const Congrates = (props: Iprops) => {
    return (
        <div className={styles.confetti} >
            {[...Array(n)].map((e, i) => <div className={styles.confettiPiece} />)}
            <span className={styles.title}>{props.title}</span>
            <br></br>
            <button className={`${styles.button} ${styles.buttonwiggle} ${styles.title}`} onClick={() => {
                window.location.reload();
            }}>Play again</button>
        </div>
    )
};

export default Congrates;