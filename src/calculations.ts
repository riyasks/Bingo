import { Theme } from "./Interfaces/IBingo";
import { faq } from "./questions";

const shuffleArray=(array: string[]) =>{
    var temp = [];
    var len=array.length;
    while(len){
       temp.push(array.splice(Math.floor(Math.random()*array.length),1)[0]);
       len--;
    }
    return temp;
 };


const groupBy = (array: any, key: string) => {
    return array
        .reduce((hash: any, obj: any) => {
            if (obj[key] === undefined) return hash;
            return Object.assign(hash, { [obj[key]]: (hash[obj[key]] || []).concat(obj) })
        }, {})
};

const createGame = (chunks: number): Theme[][] => {
    var themes: Theme[] = [];
    var _faq = shuffleArray(faq);
    for (let i = 1; i < _faq.length; i++) {
        themes.push({
            text: _faq[i],
            isActive: false,
            id: i
        })
    }

    var dimensionArray: Theme[][] = [];
    for (let x = 0; x < themes.length / chunks; x++) {
        dimensionArray.push(themes.slice(x * chunks, x * chunks + chunks))
    }
    return dimensionArray;
}


export {
    createGame,
    groupBy,

}