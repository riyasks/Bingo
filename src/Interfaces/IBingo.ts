export interface Theme {
    text: string;
    isActive: boolean;
    id: number;
  }

export interface IGame{
  user: string;
  row: number;
  col: number;
}