import React from 'react';
import { render, screen } from '@testing-library/react';
import PlayGround from './PlayGround';

test('renders learn react link', () => {
  render(<PlayGround />);
  const linkElement = screen.getByText(/Bingo/i);
  expect(linkElement).toBeInTheDocument();
});
